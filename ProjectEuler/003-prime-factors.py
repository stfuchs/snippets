from math import *

def getPrimesUpto(n):
    primes = []
    numbers = range(2,n);
    while (len(numbers) != 0):
        primes.append(numbers[0])
        p = primes[-1]
        numbers[:] = [n for n in numbers if (n % p) != 0 ]

    return primes

def getMaxPrimeFactorOf(z):
    primes = getPrimesUpto(int(ceil(sqrt(z))))
    for p in reversed(primes):
        if (z % p) == 0:
            return p

def faster(number):
    primes = set([2])
    value = 3
    while value < sqrt(number):
        isPrime = True
        for k in primes:
            if value % k == 0:
                isPrime = False
                value += 2

        if isPrime:
            primes.add(value)
            if number % value == 0:
                print value
                number /= value
                
    print number

# n = 600,851,475,143
#print getPrimesUpto(100)
#print getMaxPrimeFactorOf(600851475143)
#print getMaxPrimeFactorOf(16877340929)
#faster(16877340929)
faster(91)
#faster(600851475143)
