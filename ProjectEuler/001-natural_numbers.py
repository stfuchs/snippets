def sum_multiples35(limit):
    res = 0
    for i in range(1,limit):
        if  (i % 3) == 0:
            res += i

    return res

def mcp1(n,m):
    fl = m/n
    return n * fl * (fl+1) / 2

print sum_multiples35(1000)

print mcp1(3,999) + mcp1(5,999) - mcp1(15,999)
