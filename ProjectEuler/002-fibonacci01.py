from math import *

phi = (1.0 + sqrt(5)) * 0.5
phi3 = phi**3
sqrt5 = sqrt(5)
print "Phi="+str(phi)

def fibonacci (n):
    t = 1
    t1 = 1
    t2 = 2
    for i in range(n):
        t = t1
        t1 = t2
        t2 = t + t1
    
    return t

def fibonaccifast (n):
    return floor( phi**n / sqrt5 + 0.5)

def fibonacci_index(z):
    return floor( log(float(z) * sqrt5 + 0.5, phi) )

def is_fibonacci(x):
    return fibonaccifast(fibonacci_index(x)) == x

def sum_of_even_numbers_until(x):
    res = 0
    fibo = 2
    idx = 3
    while (fibo < x):
        res += fibo
        idx += 3
        fibo = fibonaccifast(idx)

    return res

def sum_of_even_numbers_until_2(x):
    res = 0
    fibo = 2
    while (fibo < x):
        res += fibo
        fibo = round(fibo * phi3)

    return res

limit = 4000000

print fibonacci_index (limit)
print fibonaccifast(fibonacci_index(limit))
print sum_of_even_numbers_until(limit)
print sum_of_even_numbers_until_2(limit)
