#include <iostream>
#include <stdlib.h>
#include <sys/socket.h>
#include <netdb.h>

//#include <netinet/in.h>
#include <arpa/inet.h>

#include <ros/ros.h>

class RandomClientUDP
{
public:
  RandomClientUDP() : n_("~")
  {
    n_.param<int>("port", port_, 25010);
    n_.param<std::string>("ip", ip_, "10.0.1.236");
  }

  int init()
  {
    if ((fd_ = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
      std::cerr << "cannot create socket\n" << std::endl;
      return -1;
    }

    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port_);
    servaddr.sin_addr.s_addr = inet_addr(ip_.c_str());
    return 0;
  }

  void send(double a, double b)
  {
    size_t s = 8; // sizeof(double)
    char* buf = (char*) malloc(2*s);
    memcpy(&buf[0],&a,s);
    memcpy(&buf[s],&b,s);
    if(sendto(fd_, buf, 2*s, 0, (sockaddr*)&servaddr, sizeof(servaddr)) < 0)
    {
      std::cerr << "sendto failed" << std::endl;
    }
    else
    {
      std::cout << "Send: " << a << " and " << b << std::endl;
    }
    free(buf);
  }

private:
  sockaddr_in servaddr;
  int fd_;

  ros::NodeHandle n_;
  int port_;
  std::string ip_;
};


int main(int argc, char** argv)
{
  ros::init(argc,argv,"SimpleClient");
  RandomClientUDP r;
  if (r.init() < 0) return 0;

  ros::Rate loop_rate(1);
  double n = double(2)/double(RAND_MAX);
  while(ros::ok())
  {
    r.send(double(rand())*n-1.,double(rand())*n-1.);
    ros::spinOnce();
    loop_rate.sleep();
  }
}

