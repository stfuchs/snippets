#include <iostream>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <arpa/inet.h> // conversion between network numbers and strings

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>

#define BUFSIZE 512

class BaseServerUDP
{
public:
  BaseServerUDP(void) : n_("~")
  {
    n_.param("port", port_, 25010);
    n_.param<std::string>("ip", ip_, "0.0.0.0");
    n_.param("timeout", timeout_, 5);
  }

  int init()
  {
    if ((fd_ = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
      std::cerr << "cannot create socket\n" << std::endl;
      return -1;
    }

    timeval timeout;
    timeout.tv_sec = timeout_;
    timeout.tv_usec = 0;

    if(setsockopt(fd_,SOL_SOCKET,SO_RCVTIMEO,(char*)&timeout,sizeof(timeout))<0)
    {
      std::cerr << "set timeout option failed" << std::endl;
      return -1;
    }

    myaddr_.sin_family = AF_INET;
    inet_aton(ip_.c_str(), &myaddr_.sin_addr);
    myaddr_.sin_port = htons(port_);

    if (bind(fd_, (sockaddr *)&myaddr_, sizeof(myaddr_)) < 0)
    {
      std::cerr << "bind failed" << std::endl;
      return -1;
    }

    pub_ = n_.advertise<sensor_msgs::Joy>("/joy", 1);
    std::cout << "waiting on port " << port_
              << " for " << timeout_ << "s." << std::endl;
    return 0;
  }

  void waitForMessage()
  {
    sockaddr_in remaddr; // remote address
    socklen_t addrlen = sizeof(remaddr); // length of addresses
    int recvlen = recvfrom(fd_, buf_, BUFSIZE, 0, (sockaddr*)&remaddr, &addrlen);
    if (recvlen <= 0)
    {
      std::cerr << "no message received (timeout)" << std::endl;
      return;
    }
    std::cout << "received " << recvlen  << " bytes from "
              << inet_ntoa(remaddr.sin_addr) << std::endl;

    size_t s = 8; // sizeof(double)
    double vel, ori;
    memcpy(&vel, &buf_, s);
    memcpy(&ori, &buf_[s], s);
    // scale to -1..1

    sensor_msgs::Joy joy;
    joy.axes.resize(6);
    joy.buttons.resize(12);
    joy.axes[1] = vel; // linear
    joy.axes[2] = ori; // angular
    joy.buttons[5] = 1;
    pub_.publish(joy);
  }

private:
  sockaddr_in myaddr_; // server address
  int fd_; // socket handle
  unsigned char buf_[BUFSIZE]; //receive buffer

  ros::NodeHandle n_;
  ros::Publisher pub_;
  int port_;
  std::string ip_;
  int timeout_;
};

int main(int argc, char** argv)
{
  ros::init(argc,argv,"BaseServerUDP");

  BaseServerUDP s;
  if (s.init() < 0) return 0;

  ros::Rate loop_rate(100);
  while(ros::ok())
  {
    s.waitForMessage();
    ros::spinOnce();
    loop_rate.sleep();
  }
}
