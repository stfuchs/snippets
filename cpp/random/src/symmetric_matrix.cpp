#include <iostream>
#include <Eigen/Core>

enum{ N = 5 };

int main()
{
  int data[ (N-1)*N/2 ];
  int* p = &data[0];

  for(int i=0; i<N; ++i)
  {
    for(int j=0; j<=i; ++j) 
    {
      std::cout << ".\t";
    }
    for(int j=i+1; j<N; ++j)
    {
      *p++ = i*N+j;
      std::cout << i*N+j << "\t";
    }
    std::cout << std::endl;
  }
  int* q = &data[0];
  while(q!=p)
  {
    std::cout << *q++ << " ";
  }
  std::cout << std::endl;
  Eigen::Matrix<float,N,N> m = Eigen::Matrix<float,N,N>::Zero();
  int l = (N-1)*N/2;
  int n = .5+.5*sqrt(1.+8.*l);
  p = &data[0];
  for (int i=0; i<N; ++i)
  {
    for (int j=i+1; j<N; ++j)
    {
      m(i,j) = m(j,i) = exp(-*p++);
    }
  }
  std::cout << m << std::endl;

  return 0;
}
