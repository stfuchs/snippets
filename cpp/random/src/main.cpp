#include <iostream>
#include <vector>

struct Foo
{
  int a;
};

struct Bar
{
  Foo foo;
  std::string name;
};

struct FooBar : Bar
{
  int a;
  int b;
};


int main()
{
  std::vector<int> vec = {1,2,3,4,10};
  Foo foo = { 1 };
  Bar bar = { foo, "bar" };
  FooBar foobar;
  foobar.a = 2;
  foobar.b = 3;
  foobar.foo = foo;
  foobar.name = "foobar";
  return 0;
}
