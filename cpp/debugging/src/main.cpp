#include <iostream>

struct Foo
{
  int a;
};

struct FooBar : Foo
{
  int b;
};


int main()
{
  Foo foo = { 1 };
  FooBar bar;
  bar.a = 2;
  bar.b = 3;


  return 0;
}
