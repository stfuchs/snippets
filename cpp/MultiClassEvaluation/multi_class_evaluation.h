template<typename LabelT> class MultiClassEvaluation
{
  public:
    MultiClassEvaluation (const int & size)
    {
      for (int i=0;i<size;i++) labels_by_int_[i] = i;
      std::vector<int> sub(size,0);
      confusion_(size,sub);
    };
    
    MultiClassEvaluation (const std::vector<LabelT> & labels)
    {
      for (int i=0;i<labels.size();i++) labels_[labels[i]] = i;
      std::vector<int> sub(labels.size(),0);
      confusion_(labels.size(),sub);
    };
    
    MultiClassEvaluation (const std::vector<LabelT> & labels, 
      const std::vector<std::string> & names) : names_(names)
    { 
      for (int i=0;i<labels.size();i++) labels[labels[i]] = i;
      std::vector<int> sub(labels.size(),0);
      confusion_(labels.size(),sub);
    };
      
    inline void 
    increment (const LabelT & expectation,
               const LabelT & prediction)
    {
      confusion_.at(labels_[expectation]).at(labels_[prediction]) ++;
    }
    
    inline void 
    incrementAtPosByN (const size_t & expectation,
      const size_t & prediction,
      const int n = 1)
    {
      confusion_.at(expectation).at(prediction) += n;
    }
    
    inline int
      getPosExpectation (const LabelT & label) const
    {
      int i,sum=0;
      for(i=0;i<confusion_.size();i++) sum += confusion_.at(labels_[label]).at(i); 
      return sum; 
    }
    
    inline int
      getPosPrediction (const LabelT & label) const;
    {
      int i,sum=0;
      for(i=0;i<confusion_.size();i++) sum += confusion_.at(i).at(labels_[label]); 
      return sum; 
    }
    
    inline float
      getPrecision (const LabelT & label) const;
      
    inline float
      getRecall (const LabelT & label) const;
      
    inline float
      getFMeasure (const LabelT & label) const;
      
    inline float
      getMacroAvgPrecision () const;
      
    inline float
      getMacroAvgRecall () const;
      
    inline float
      getMacroAvgFMeasure () const;
    
  protected:
    std::map<LabelT, int> labels_;
    std::vector<std::string> names_;
    
    std::vector<std::vector<int> > confusion_;
    
}