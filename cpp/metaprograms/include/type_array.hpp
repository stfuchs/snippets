#ifndef TYPE_ARRAY_HPP__
#define TYPE_ARRAY_HPP__

#include <type_traits>

template<typename... Ts>
struct type_array
{
  typedef type_array<> next;
  typedef void sub_type;
  enum { N = 0 };
};

template<typename T, typename... Ts>
struct type_array<T,Ts...> : type_array<Ts...>
{
  typedef type_array<Ts...> next;
  typedef T sub_type;
  enum { N = next::N+1 };
};

template<int N, typename T, typename... Ts>
struct repeat : repeat<N-1, T, T, Ts...> { };

template<typename T, typename... Ts>
struct repeat<0, T, Ts...>
{
  typedef type_array<Ts...> result;
};



#endif
