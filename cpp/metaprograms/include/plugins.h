class PluginA
{
public:
  PluginA(const std::string& name) : name_(name) {}

  inline void print(int n) const
  {
    std::cout << "TypeA | " << name_ << " | " << n << std::endl;
  }

private:
  std::string name_;
};


class PluginB
{
public:
  PluginB(const std::string& name) : name_(name) {}

  inline void print(int n) const
  {
    std::cout << "TypeB | " << name_ << " | " << n << std::endl;
  }

private:
  std::string name_;
};


class PluginC
{
public:
  PluginC(const std::string& name) : name_(name) {}

  inline void print(int n) const
  {
    std::cout << "TypeC | " << name_ << " | " << n << std::endl;
  }

private:
  std::string name_;
};
