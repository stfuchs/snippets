#ifndef MULTI_TYPE_CONTAINER_HPP__
#define MULTI_TYPE_CONTAINER_HPP__

struct NullT {};

// Multi-Type-Container implementation
template<template<typename,typename...> class Container, typename... Ts>
struct MultiTypeContainer
{
  typedef NullT base_type;
  enum{ N = 0 };
};

template<template<typename,typename...> class Container, typename T, typename... Ts>
struct MultiTypeContainer<Container,T,Ts...> : MultiTypeContainer<Container,Ts...>
{
  typedef Container<T> container_type;
  typedef T value_type;
  typedef MultiTypeContainer<Container,Ts...> base_type;

  enum{ N = base_type::N+1 };

  container_type c;
};


// Traits for Multi-Type-Container
template<typename MTC, typename T, typename T2 = typename MTC::value_type>
struct MultiTypeTraits
{
  typedef typename MultiTypeTraits<typename MTC::base_type, T>::base_type base_type;
  typedef typename base_type::container_type container_type;
  typedef typename container_type::iterator iterator;
  typedef typename container_type::const_iterator const_iterator;
};

template<typename MTC, typename T>
struct MultiTypeTraits<MTC,T,T>
{
  typedef MTC base_type;
  typedef typename MTC::container_type container_type;
  typedef typename container_type::iterator iterator;
  typedef typename container_type::const_iterator const_iterator;
};

// Getter functions 
template<typename T, typename MTC>
inline typename MultiTypeTraits<MTC,T>::container_type& get(MTC& mtc)
{
  typedef typename MultiTypeTraits<MTC,T>::base_type container;
  return static_cast<container &>(mtc).c;
}

template<typename T, typename MTC>
inline typename MultiTypeTraits<MTC,T>::container_type const& get(MTC const& mtc)
{
  typedef typename MultiTypeTraits<MTC,T>::base_type container;
  return static_cast<container const &>(mtc).c;
}

template<typename MTC, int I = MTC::N>
struct for_each_container
{
  template<typename FuncT, typename... ArgsT>
  inline void operator() (MTC& mtc, FuncT f, ArgsT&... args)
  {
    //printf("%s\n", __PRETTY_FUNCTION__);
    f(mtc.c, args...);
    for_each_container<typename MTC::base_type>()(mtc,f,args...);
  }
};

template<typename MTC>
struct for_each_container<MTC,1>
{
  template<typename FuncT, typename... ArgsT>
  inline void operator() (MTC& mtc, FuncT f, ArgsT&... args)
  {
    //printf("%s\n", __PRETTY_FUNCTION__);
    f(mtc.c, args...);
  }
};

struct for_each_element
{
  template<typename CT, typename FuncT, typename... ArgsT>
  inline void operator() (CT& cont, FuncT f, ArgsT& ... args)
  {
    for(typename CT::iterator it = cont.begin(); it != cont.end(); ++it)
    {
      f(it, args...);
    }
  }
};


// inner foreach iterations
template<typename MTC, int I = MTC::N>
struct for_each_container_inner
{
  template<typename OuterT, typename FuncT, typename... ArgsT>
  inline void operator() (OuterT& out, MTC& mtc, FuncT f, ArgsT& ... args)
  {
    f(out, mtc.c, args...);
    for_each_container_inner<typename MTC::base_type>()(out,mtc,f,args...);
  }
};

template<typename MTC>
struct for_each_container_inner<MTC,1>
{
  template<typename OuterT, typename FuncT, typename... ArgsT>
  inline void operator() (OuterT& out, MTC& mtc, FuncT f, ArgsT&... args)
  {
    f(out, mtc.c, args...);
  }
};

struct for_each_element_inner
{
  template<typename OuterT, typename CT, typename FuncT, typename...ArgsT>
  inline void operator() (OuterT& out, CT& cont, FuncT f, ArgsT&... args)
  {
    for(typename CT::iterator it = cont.begin(); it != cont.end(); ++it)
    {
      f(out, it, args...);
    }
  }
};

template<typename MTC, typename FuncT>
inline void foreach(MTC& mtc, FuncT f)
{
  for_each_container<MTC>()(
    mtc, for_each_element(), f);
}


template<typename MTC, typename FuncT>
inline void foreach2(MTC& mtc, FuncT f)
{
  for_each_container<MTC>()(
    mtc, for_each_element(), 
    for_each_container_inner<MTC>(),
    mtc, for_each_element_inner(), f);
}


#endif
