#include <iostream>
#include <slots.hpp>
#include <plugins.h>

class Printer
{
public:
  template<typename PluginT>
  inline static void runPlugin(const PluginT& plugin, int i)
  {
    plugin.print(i);
  }
};



int main(int argc, char** argv)
{
  std::cout << "hallo" << std::endl;

  PluginA emma("Emma");
  PluginB karl("Karl");
  PluginC futz("Futz");

  PluginA anna("Anna");
  PluginB bill("Bill");

  Slots<PluginA,PluginB,PluginC> m3(emma,karl,futz);
  m3.run<Printer>(1);
  std::cout << std::endl;

  Slots<PluginA> m1(anna);
  m1.run<Printer>(2);
  std::cout << std::endl;

  Slots<> m0;
  m0.run<Printer>(3);
  std::cout << std::endl;

  tiePlugins(bill,anna,futz,karl,emma).run<Printer>(4);
  std::cout << std::endl;

  std::cout << sizeof(m0) << std::endl;
  std::cout << sizeof(m1) << std::endl;
  std::cout << sizeof(m3) << std::endl;
  std::cout << sizeof(tiePlugins(bill,anna,futz,karl,emma)) << std::endl;

  return 0;
}
