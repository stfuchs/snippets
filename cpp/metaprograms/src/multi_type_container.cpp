#include <iostream>
#include <vector>
#include <list>
#include <unordered_map>
#include <typeinfo>

#include "multi_type_container.hpp"

template<typename T>
struct Foo
{
  Foo(T const& t) : member(t) {}
  T member;

  friend std::ostream& operator<< (std::ostream& os, const Foo<T>& foo) { 
    return os << foo.member;
  }
};

struct Print
{
  template<typename IterT>
  inline void operator() (IterT& it)
  {
    std::cout << *it << std::endl;
  }
};


struct PrintDist
{
  template<typename IterT1, typename IterT2>
  void operator() (IterT1& it1, IterT2& it2)
  {
    std::cout << *it1 << " - " << *it2 << std::endl;
  }
};


template<typename T>
T variadic(T v)
{
  return v;
}

template<typename T, typename... ArgsT>
T variadic(T v, ArgsT... args)
{
  return v + variadic<ArgsT...>(args...);
}

template<typename T>
void variadic_containers(T& c)
{
  std::cout << &*c.begin() << std::endl;
}

template<typename T, typename... ArgsT>
void variadic_containers(T& c, ArgsT &... args)
{
  std::cout << &*c.begin() << std::endl;
  variadic_containers<ArgsT...>(args...);
}


template<typename ContT, typename IterT>
struct elements_container
{
  void operator() (ContT& c, IterT& it_outer)
  {
    typename ContT::iterator it_inner = c.begin();
    for(;it_inner!=c.end();++it_inner)
    {
      PrintDist()(it_outer,it_inner);
    }
  }
};

template<typename ContT>
struct elements_container<ContT, typename ContT::iterator>
{
  void operator() (ContT& c, typename ContT::iterator& it_outer)
  {
    typename ContT::iterator it_inner = it_outer;
    for(; it_inner!=c.end(); ++it_inner)
    {
      PrintDist()(it_outer,it_inner);
    }
  }
};

struct elements_inner
{
  template<typename ContT, typename IterT>
  void operator() (ContT& c, IterT& it_outer)
  {
    elements_container<ContT,IterT>()(c,it_outer);
  }
};

struct elements_outer
{
  template<typename ContT, typename MTC>
  void operator() (ContT& c, MTC& mtc)
  {
    typename ContT::iterator it = c.begin();
    typedef typename MultiTypeTraits<MTC,typename ContT::value_type>::base_type container;
    for(;it!=c.end();++it)
    {
      for_each_container<container>()(mtc,elements_inner(),it);
    }
  }
};


int main()
{
  typedef int T1;
  typedef double T2;
  typedef Foo<double> T3;
  typedef std::string T4;
  typedef MultiTypeContainer<std::vector,T1,T2,T3,T4> MTC;
  MTC mtc;
  get<T1>(mtc).push_back(1);
  get<T2>(mtc).push_back(2.2);
  get<T2>(mtc).push_back(2.3);
  get<T2>(mtc).push_back(2.4);
  get<T2>(mtc).push_back(2.5);
  get<T3>(mtc).push_back(T3(3.03));
  get<T3>(mtc).push_back(T3(3.04));
  get<T4>(mtc).push_back("hello");
  get<T4>(mtc).push_back("world");
  std::cout << get<T1>(mtc).size() << std::endl;
  std::cout << get<T2>(mtc).size() << std::endl;
  std::cout << get<T3>(mtc).size() << std::endl;
  //foreach(mtc, Print());
  std::cout << variadic(1,2,3,4,5) << std::endl;
  //foreach2(mtc, PrintDist());  
  for_each_container<MTC>()(mtc,elements_outer(),mtc);

  return 0;
}
