#include <iostream>
#include <vector>

#include "multi_type_container.hpp"

enum TypeId { T1, T2 };

struct ElemId
{
  unsigned int off;
  TypeId tid;

  friend std::ostream& operator<< (std::ostream& os, const ElemId& el) {
    return os << el.tid <<":"<< el.off;
  }
};

struct ElemT1
{
  int value;
  ElemId id;

  friend std::ostream& operator<< (std::ostream& os, const ElemT1& el) {
    return os << "["<<el.id<<"] " << el.value;
  }
};

struct ElemT2
{
  double value;
  ElemId id;

  friend std::ostream& operator<< (std::ostream& os, const ElemT2& el) {
    return os << "["<<el.id<<"] " << el.value;
  }
};

template<typename T>
struct Traits {};

template<>
struct Traits<int>
{
  static const TypeId Id = TypeId::T1;
  typedef ElemT1 Elem;
};

template<>
struct Traits<double>
{
  static const TypeId Id = TypeId::T2;
  typedef ElemT2 Elem;
};


template<typename T>
typename Traits<T>::Elem create(T value)
{
  static unsigned int off = 0;
  ElemId id = { off++, Traits<T>::Id };
  typename Traits<T>::Elem e = { value, id };
  return e;
}

struct Print
{
  template<typename IterT>
  inline void operator() (IterT& it)
  {
    std::cout << *it << std::endl;
  }
};


int main()
{
  ElemT1 a1 = create(1);
  ElemT1 a2 = create(2);
  ElemT1 a3 = create(3);
  ElemT2 b1 = create(1.1);
  ElemT2 b2 = create(2.2);
  ElemT2 b3 = create(3.3);

  typedef MultiTypeContainer<std::vector,ElemT1,ElemT2> MTC;
  MTC mtc;
  get<ElemT1>(mtc).push_back(a1);
  get<ElemT1>(mtc).push_back(a2);
  get<ElemT1>(mtc).push_back(a3);
  get<ElemT2>(mtc).push_back(b1);
  get<ElemT2>(mtc).push_back(b2);
  get<ElemT2>(mtc).push_back(b3);
  foreach(mtc, Print());
  return 0;
}
