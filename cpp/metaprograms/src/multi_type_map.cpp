#include <iostream>
#include <vector>
#include <unordered_map>
#include <type_traits>

#include "multi_type_map.hpp"

template<typename T> struct Traits { };

template<typename T>
struct Foo
{
  typedef typename Traits<Foo<T> >::id_type id_type;
  T value;
  id_type id;

  friend std::ostream& operator<< (std::ostream& os, const Foo<T>& foo) {
    return os << "["<<foo.id<<"] " << foo.value;
  }
};

template<>
struct Traits<Foo<int> >
{
  typedef std::string id_type;
  static const char* text_ids[];
  static id_type new_id(unsigned int i) { return std::string(text_ids[i]); }
};
const char* Traits<Foo<int> >::text_ids[] = { "abc", "xyz", "foo", "bar" };

template<>
struct Traits<Foo<double> >
{
  typedef unsigned int id_type;
  static id_type new_id(unsigned int i) { return i; }
};


template<typename T>
Foo<T> create(T value)
{
  static unsigned int off = 0;
  typename Foo<T>::id_type id = Traits<Foo<T> >::new_id(off++);
  Foo<T> foo = { value, id };
  return foo;
}

struct Print
{
  template<typename IterT>
  inline void operator() (IterT& it)
  {
    std::cout << it->first << " " << it->second << std::endl;
  }
};

struct PairPrinter
{
  template<typename T>
  void operator() (T& pair)
  {
    std::cout << pair.first << " " << pair.second << std::endl;
  }
};

struct VectorPrinter
{
  template<typename T>
  void operator() (T& cont)
  {
    for(typename T::iterator it = cont.begin(); it != cont.end(); ++it)
    {
      std::cout << *it << " " << std::endl;
    }
  }
};




int main()
{
  typedef Foo<int> T1;
  typedef Foo<double> T2;
  typedef typename Traits<T1>::id_type T1Id;
  typedef typename Traits<T2>::id_type T2Id;
  
  T1 a1 = create(1);
  T1 a2 = create(2);
  T1 a3 = create(3);
  T2 b1 = create(1.1);
  T2 b2 = create(2.2);
  T2 b3 = create(3.3);
/*
  typedef MultiTypeMap<std::unordered_map, T1Id, T1, T2Id, T2> MTM;
  MTM mtm;
  get(mtm, a1.id) = a1;
  get(mtm, a2.id) = a2;
  get(mtm, a3.id) = a3;
  get(mtm, b1.id) = b1;
  get(mtm, b2.id) = b2;
  get(mtm, b3.id) = b3;

  foreach(mtm, Print());
*/
/*
  typedef multi_pair< zip<char,int,double,std::string> > MP;
  MP mp;
  cast_index<0>(mp) = std::make_pair(100,1);
  cast_index<1>(mp) = std::make_pair(2.3,"test");
  for_each_map<MP>()(mp,PairPrinter());
  typedef test<std::vector,1,int,float> test1;
  typedef test<std::pair,2,int,float,char,char> test2;
*/
  typedef MultiType<std::vector, 1, T1, T2> MultiVec;
  typedef MultiType<std::pair, 2, T1Id, T1, T2Id, T2> MultiPair;
  MultiPair mp;
  cast_index<0>(mp) = std::make_pair(a1.id,a1);
  cast_index<1>(mp) = std::make_pair(b1.id,b1);
  for_each_subtype<MultiPair>()(mp,PairPrinter());

  MultiVec mv;
  cast_index<0>(mv).push_back(a1);
  cast_index<0>(mv).push_back(a2);
  cast_index<0>(mv).push_back(a3);
  cast_index<1>(mv).push_back(b1);
  cast_index<1>(mv).push_back(b2);
  cast_index<1>(mv).push_back(b3);
  for_each_subtype<MultiVec>()(mv,VectorPrinter());
  

  return 0;
}
