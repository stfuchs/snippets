#include <iostream>
#include <vector>

template<int I>
struct counter : counter<I-1>
{
  typedef counter<I-1> base_type;
  enum { N = I };
};

template<>
struct counter<0>
{
  enum { N = 0 };
};


template<int N, int M>
struct equal_int 
{
  enum { res = false };
};

template<int N>
struct equal_int<N,N>
{
  enum { res = true };
};

template<typename T1, typename T2>
struct equal_type
{
  enum { res = false };
};

template<typename T>
struct equal_type<T,T>
{
  enum { res = true };
};


template<int N, typename T, bool B>
struct strip_base_by_number
{ };

template<int N, typename T>
struct strip_base_by_number<N,T,false>
{
  typedef typename T::base_type base;
  typedef typename strip_base_by_number<N, base, equal_int<N, base::N>::res >::res res;
};


template<int N, typename T>
struct strip_base_by_number<N,T,true>
{
  typedef T base;
  typedef T res;
};

template<int N, typename T>
struct find_base
{
  typedef typename strip_base_by_number<N, T, equal_int<N, T::N>::res >::res result;
};

int main()
{
  typedef counter<4> T4;
  //T4 c4;
  typedef typename find_base<2,T4>::result B2;
  typedef typename find_base<0,T4>::result B3;
  std::cout << B2::N << " " << B3::N << std::endl;
  return 0;
}

