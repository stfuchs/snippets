#include <iostream>
#include <deque>
#include <unordered_map>

#include "multi_type_map.hpp"
#include <Eigen/Core>

template<typename Tracker>
struct BaseId 
{
  BaseId() : _id(-1) { }
  BaseId(int id) : _id(id) { }

  inline int& operator() () { return _id; }
  inline bool is_valid() { return _id != -1; }

  friend std::ostream& operator<< (std::ostream& os, const BaseId<Tracker>& id) {
    return os << Tracker::type_id <<":" << id._id;
  }

  int const _id;
};

namespace std
{
  template<typename T>
  struct hash<BaseId<T> >
  {
    std::size_t operator() (const BaseId<T>& key) const
    {
      return std::hash<int>()(key._id);
    }
  };
}

template<typename T>
inline const bool operator==  (const BaseId<T>& lhs, const BaseId<T>& rhs) {
  return lhs._id == rhs._id;
}

template<typename T1, typename T2>
inline const bool operator==  (const BaseId<T1>& lhs, const BaseId<T2>& rhs) {
  return false;
}

template<typename T>
inline const bool operator!=  (const BaseId<T>& lhs, const BaseId<T>& rhs) {
  return !operator== (lhs._id,rhs._id);
}

template<typename T1, typename T2>
inline const bool operator!=  (const BaseId<T1>& lhs, const BaseId<T2>& rhs) {
  return true;
}

template<typename T>
inline const bool operator<  (const BaseId<T>& lhs, const BaseId<T>& rhs) {
  return lhs._id < rhs._id;
}

template<typename T>
inline const bool operator>  (const BaseId<T>& lhs, const BaseId<T>& rhs) {
  return  operator< (rhs, lhs);
}

template<typename T>
inline const bool operator<= (const BaseId<T>& lhs, const BaseId<T>& rhs) {
  return !operator> (lhs, rhs);
}

template<typename T>
inline const bool operator>= (const BaseId<T>& lhs, const BaseId<T>& rhs) {
  return !operator< (lhs, rhs);
}


template<typename Traits, typename Tracker>
struct Trajectory
{
  typedef Traits type;
  typedef Tracker tracker_type;
  typedef typename Traits::StateT StateT;
  typedef typename Traits::ValueT ValueT;
  typedef typename Traits::TimeT TimeT;
  typedef BaseId<Tracker> IdT;

  Trajectory(IdT const& id_) : id(id_) {}

  IdT const id;
  std::deque<TimeT> t;
  std::deque<StateT> x;
};

struct DefaultTraits
{
  typedef float ValueT;
  typedef float StateT;
  typedef float TimeT;
};

struct PointTraits : DefaultTraits {};

struct PoseTraits : DefaultTraits
{
  struct StateT
  {
    float point;
    float orientation;
  };
};

struct LK_Tracker : Trajectory<PointTraits,LK_Tracker>
{
  typedef Trajectory<PointTraits,LK_Tracker> Base;
  LK_Tracker(IdT const& id_) : Base(id_) {}

  static const int type_id = 0;
  static const unsigned int n_min = 3;
  static const unsigned int n_max = 1000;

  static const float weight;// = .5;
  static const typename Base::TimeT timespan;// = 3.;
};
const float LK_Tracker::weight = .5;
const typename LK_Tracker::Base::TimeT LK_Tracker::timespan = 3.;



struct QR_Tracker : Trajectory<PoseTraits,QR_Tracker>
{
  typedef Trajectory<PoseTraits,QR_Tracker> Base;
  QR_Tracker(IdT const& id_) : Base(id_) {}

  static const int type_id = 1;
  static const unsigned int n_min = 3;
  static const unsigned int n_max = 1000;

  static const float weight;
  static const typename Base::TimeT timespan;
};
const float QR_Tracker::weight = .8;
const typename QR_Tracker::Base::TimeT QR_Tracker::timespan = 3.;

/***************************************************************************************
 *  Type Functions
 **************************************************************************************/

template<typename T>
struct id_traits {};

template<>
struct id_traits<typename LK_Tracker::IdT>
{
  typedef LK_Tracker trajectory;
};

template<>
struct id_traits<typename QR_Tracker::IdT>
{
  typedef QR_Tracker trajectory;
};

// traits defining resulting types of different trajectory types
template<typename T1, typename T2>
struct trajectory_type_promotion
{
  typedef float res_distance_type;
  typedef float res_time_type;
  typedef float res_state_type;
};

template<typename T>
struct trajectory_type_promotion<T,T>
{
  typedef typename T::ValueT res_distance_type;
  typedef typename T::TimeT res_time_type;
  typedef typename T::StateT res_state_type;
};


/***************************************************************************************
 *  Policy Functions
 **************************************************************************************/
template<typename T1, typename T2>
struct trajectory_policy
{
  typedef typename trajectory_type_promotion<T1,T2>::res_distance_type DistanceT;
  typedef typename trajectory_type_promotion<T1,T2>::res_time_type TimeT;
  typedef typename trajectory_type_promotion<T1,T2>::res_state_type StateT;

  inline static DistanceT distance(typename T1::StateT const& xi, typename T2::StateT const& xj) {
    return xi-xj;
  }

  inline static StateT lin_inter(typename T1::StateT const& x1,
                                 typename T2::StateT const& x2,
                                 TimeT const & a) {
    return a*x1 + (1.-a)*x2;
  }
};

template<>
typename trajectory_policy<PoseTraits,PoseTraits>::DistanceT
trajectory_policy<PoseTraits,PoseTraits>::distance(
  PoseTraits::StateT const& xi,PoseTraits::StateT const& xj) {
  return xi.point - xj.point;
}

template<>
typename trajectory_policy<PointTraits,PoseTraits>::DistanceT
trajectory_policy<PointTraits,PoseTraits>::distance(
  PointTraits::StateT const& xi,PoseTraits::StateT const& xj) {
  return xi - xj.point;
}

template<>
typename trajectory_policy<PoseTraits,PointTraits>::DistanceT
trajectory_policy<PoseTraits,PointTraits>::distance(
  PoseTraits::StateT const& xi,PointTraits::StateT const& xj) {
  return xi.point - xj;
}

template<>
typename trajectory_policy<PoseTraits,PoseTraits>::StateT
trajectory_policy<PoseTraits,PoseTraits>::lin_inter(
  PoseTraits::StateT const& x1,
  PoseTraits::StateT const& x2,
  typename trajectory_policy<PoseTraits,PoseTraits>::TimeT const& a) {
  return PoseTraits::StateT{a*x1.point + (1.f-a)*x2.point};
}



template<typename T1>
struct DistanceCalculator
{
  typedef typename T1::TimeT TimeT11;
  typedef typename T1::StateT StateT11;

  T1 const& fi;
  TimeT11 dt_inv;
  
  DistanceCalculator(T1 const& fi_) : fi(fi_) {
    dt_inv = 1./(fi.t[0] - fi.t[1]);
  }

  template<typename T2, typename D>
  void operator() (T2 const& fj, D& distances) const
  {
    typedef typename T1::type BT1;
    typedef typename T2::type BT2;
    typedef typename trajectory_type_promotion<BT1,BT2>::res_time_type TimeT12;
    typedef typename trajectory_type_promotion<BT1,BT2>::res_distance_type DistanceT12;
    if(fj.id==fi.id) return;

    unsigned int i = 0;
    while(fj.t[i] >= fi.t[1] && i<fj.t.size())
    {
      TimeT12 a = (fj.t[i] - fi.t[1]) * dt_inv;
      StateT11 xi = trajectory_policy<BT1,BT1>::lin_inter(fi.x[0],fi.x[1],a);
      DistanceT12 d = trajectory_policy<BT2,BT1>::distance(fj.x[i], xi);

      get(get(distances,fj.id)[i],fi.id) = d;
      ++i;
    }
  }
};

struct MatrixCalculator
{
  template<typename T1, typename T2, typename D>
  void operator() (T1 const& outer, T2 const& inner, D const& distances,
                   std::vector<float>& result)
  {
    typedef typename trajectory_type_promotion<typename T1::type, typename T2::type>::
      res_distance_type DistanceT;
    
    int n = 0;
    DistanceT sum = 0;
    DistanceT sum_sqr = 0;
    auto& que_outer = find(distances,outer.id);
    for(unsigned int i=0; i<que_outer.size(); ++i) // iterate outer distances
    {
      if (outer.t[i] >= inner.t.front()) continue;
      if (outer.t[i] < inner.t.back()) break;
      DistanceT const d = find(que_outer[i],inner.id);
      ++n;
      sum += d;
      sum_sqr += d*d;
    }
    auto& que_inner = find(distances,inner.id);
    for(unsigned int i=0; i<que_inner.size(); ++i) // iterate inner distances
    {
      if (inner.t[i] >= outer.t.front()) continue;
      if (inner.t[i] < outer.t.back()) break;
      DistanceT const d = find(que_inner[i],outer.id);
      ++n;
      sum += d;
      sum_sqr += d*d;
    }
    if(n!=0) {
      float n_inv = 1./n;
      result.push_back( exp(-(sum_sqr - sum*sum*n_inv)*n_inv) );
    }
    else {
      result.push_back( .5 );
    }
  }
};

struct PrintIds
{
  template<typename T>
  void operator() (T const& t) { std::cout << t.id << std::endl; }
};


template<typename... Ts>
struct Kernel
{
  typedef type_array<Ts...> traj_types;
  typedef type_array<typename Ts::IdT...> id_types;

  template<typename T>
  using dist_types = type_array< 
    typename trajectory_type_promotion<typename T::type,typename Ts::type>::res_distance_type... >;

  template<typename T>
  using DistanceSet = MultiTypeDuo<std::unordered_map,id_types,dist_types<T> >;

  template<typename T>
  using DistanceQue = std::deque<DistanceSet<T> >;


  MultiTypeDuo<std::unordered_map,id_types,traj_types> data;
  MultiTypeDuo<std::unordered_map,id_types,type_array<DistanceQue<Ts>...> > distances;


  template<typename IdT>
  void newTrajectory(IdT const& id,
                     typename id_traits<IdT>::trajectory::StateT const& x_new,
                     typename id_traits<IdT>::trajectory::TimeT const& t_new)
  {
    typedef typename id_traits<IdT>::trajectory T;
    T& f = set(data,id);
    f.x.push_front(x_new);
    f.t.push_front(t_new);
    DistanceQue<T>& d = set(distances,id, DistanceQue<T>());
    d.push_front(DistanceSet<T>()); // push empty distance_set onto deque
  }

  template<typename IdT>
  void updateTrajectory(IdT const& id,
                        typename id_traits<IdT>::trajectory::StateT const& x_new,
                        typename id_traits<IdT>::trajectory::TimeT const& t_new)
  {
    typedef typename id_traits<IdT>::trajectory T;
    T& f = find(data,id);
    f.x.push_front(x_new);
    f.t.push_front(t_new);
    DistanceQue<T>& d = find(distances,id);
    d.push_front(DistanceSet<T>()); // push empty distance_set onto deque
    while( (t_new - f.t.back() > T::timespan || f.t.size() > T::n_max) && f.t.size() > T::n_min)
    {
      f.t.pop_back();
      f.x.pop_back();
      d.pop_back();
    }
    // update distances of all other trajectories to the new time interval (t,t-1)
    // of this trajectory:
    foreach_value(data,DistanceCalculator<T>(f),distances);
  }

  void computeKernelMatrix()
  {
    std::vector<float> result;
    foreach_twice(data,MatrixCalculator(),distances,result);
    int n = .5+.5*sqrt(1.+8.*result.size());
    Eigen::MatrixXf m = Eigen::MatrixXf::Ones(n,n);
    typename std::vector<float>::iterator it = result.begin();
    for(int i=0;i<n;++i) for(int j=i+1;j<n; ++j) m(i,j) = m(j,i) = *it++;

    std::cout << m << std::endl;
    foreach_value(data,PrintIds());
  }
};

int main()
{
  typedef typename LK_Tracker::IdT lk_id;
  typedef typename LK_Tracker::StateT lk_x;
  typedef typename LK_Tracker::TimeT lk_t;
  typedef typename QR_Tracker::IdT qr_id;
  typedef typename QR_Tracker::StateT qr_x;
  typedef typename QR_Tracker::TimeT qr_t;

  Kernel<LK_Tracker,QR_Tracker> k;
  k.newTrajectory(lk_id(0),lk_x(.5),lk_t(.01));
  k.newTrajectory(lk_id(1),lk_x(.6),lk_t(.01));
  k.newTrajectory(lk_id(5),lk_x(.4),lk_t(.01));

  k.newTrajectory(qr_id(0),qr_x({.5}),qr_t(.01));
  k.newTrajectory(qr_id(1),qr_x({.6}),qr_t(.01));
  k.newTrajectory(qr_id(5),qr_x({.4}),qr_t(.01));


  k.updateTrajectory(lk_id(1),lk_x(.4),lk_t(.02));
  k.updateTrajectory(lk_id(5),lk_x(.4),lk_t(.02));
  k.updateTrajectory(lk_id(1),lk_x(.4),lk_t(.03));

  k.updateTrajectory(qr_id(0),qr_x({.6}),qr_t(.04));
  k.updateTrajectory(qr_id(1),qr_x({.7}),qr_t(.04));
  k.updateTrajectory(qr_id(5),qr_x({.8}),qr_t(.04));

  k.computeKernelMatrix();

  /*
  typedef type_array<int,double,char,int> ta1;
  typedef repeat<ta1::N,float>::result ta2;
  std::cout << ta1::N << std::endl;
  std::cout << sizeof(by_index<ta1,0>::type::sub_type) << std::endl;
  std::cout << sizeof(by_index<ta1,1>::type::sub_type) << std::endl;
  std::cout << sizeof(by_index<ta1,2>::type::sub_type) << std::endl;
  std::cout << sizeof(by_index<ta1,3>::type::sub_type) << std::endl;
  std::cout << "\n" << ta2::N << std::endl;
  std::cout << sizeof(by_index<ta2,0>::type::sub_type) << std::endl;
  std::cout << sizeof(by_index<ta2,1>::type::sub_type) << std::endl;
  std::cout << sizeof(by_index<ta2,2>::type::sub_type) << std::endl;
  std::cout << sizeof(by_index<ta2,3>::type::sub_type) << std::endl;
  std::cout << sizeof(type_array<float,float,float,float>) << std::endl;
  */
  return 0;
}
