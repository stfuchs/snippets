#ifndef VECTOR_MATH_H_
#define VECTOR_MATH_H_

#include <iostream>
#include <string>
#include <sstream>

namespace imglib
{
  template<size_t R, size_t C, typename T>
  struct Mat
  {
    typedef Mat<R,C,T> MatT;
    static const size_t nRows;
    static const size_t nCols;
    typedef T type;

    T data[R][C];

    inline T& operator () (size_t r, size_t c) { return(data[r][c]); }

    inline MatT& operator += (const MatT& rhs)
    {
      for(size_t r=0;r<R;++r) { for(size_t c=0;c<C;++c) { this->data[r][c]+=rhs.data[r][c]; } }
      return (*this);
    }

    inline MatT& operator -= (const MatT& rhs)
    {
      for(size_t r=0;r<R;++r) { for(size_t c=0;c<C;++c) { this->data[r][c]-=rhs.data[r][c]; } }
      return (*this);
    }

    inline MatT& operator *= (const T& rhs)
    {
      for(size_t r=0;r<R;++r) { for(size_t c=0;c<C;++c) { this->data[r][c]*=rhs; } }
      return(*this);
    }

    inline void setZero()
    { for(size_t r=0;r<R;++r) { for(size_t c=0;c<C;++c) { data[r][c] = 0; } } }

    inline std::string printOneLine()
    {
      std::stringstream ss;
      for(size_t r=0;r<R;++r)
      {
        ss << "[" << data[r][0];
        for(size_t c=1;c<C;++c) { ss << ", " << data[r][c]; }
        ss << "]";
      }
      return ss.str();
    }

    inline std::string printMultiLine()
    {
      std::stringstream ss;
      for(size_t r=0;r<R;++r)
      {
        ss << "[" << data[r][0];
        for(size_t c=1;c<C;++c) { ss << ", " << data[r][c]; }
        ss << "]\n";
      }
      return ss.str();
    }
  };

  template<size_t R, size_t C, typename T> const size_t Mat<R,C,T>::nRows = R;
  template<size_t R, size_t C, typename T> const size_t Mat<R,C,T>::nCols = C;

  template<typename MatT> inline MatT operator + (MatT lhs, const MatT& rhs)
  { return( MatT( lhs += rhs ) ); }

  template<typename MatT> inline MatT operator - (MatT lhs, const MatT& rhs)
  { return( MatT( lhs -= rhs ) ); }

  template<typename MatT> inline MatT operator * (typename MatT::type lhs, MatT rhs)
  { return( MatT( rhs *= lhs ) ); }

  template<typename MatT> inline MatT operator * (MatT lhs, const typename MatT::type& rhs)
  { return( MatT( lhs *= rhs ) ); }


  template<typename T, size_t R1, size_t CR, size_t C2>
  inline Mat<R1, C2, T> operator* (const Mat<R1,CR,T>& lhs, const Mat<CR,C2,T>& rhs)
  {
    Mat<R1, C2, T> res;
    res.setZero();
    for(size_t r=0;r<R1;++r)
    {
      for(size_t c=0;c<C2;++c)
      {
        for(size_t m=0;m<CR;++m)
        {
          res.data[r][c] += lhs.data[r][m] * rhs.data[m][c];
        }
      }
    }
    return res;
  }
}

#endif
