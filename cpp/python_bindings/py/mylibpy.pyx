from libcpp.vector cimport vector
from libcpp.string cimport string
from cython.operator cimport dereference as deref
from libeigenmap cimport Map3d
cimport numpy as np

cdef extern from "../src/mylib.cpp" namespace "mylib":
    cdef cppclass MyClass:
        MyClass(int) except +
        int i_
        void hello()
        void int_vec(vector[int]&)
        void out_vec(vector[double]&)
        void T_vec[T](vector[T]&)
    cdef void ptr_vec[T](vector[T*])

cpdef print_ptr(object[np.double_t, ndim=2] v):
    cdef double* p = &v[0,0]
    cdef vector[double*] cvec
    for i in range(v.shape[0]):
        cvec.push_back(p)
        p += 3
    ptr_vec[double](cvec)

cdef class PyClass:
    cdef MyClass *thisptr
    def __cinit__(self, int i):
        self.thisptr = new MyClass(i)
        
    def __dealloc__(self):
        del self.thisptr

    def hello(self):
        """ print something nice """
        self.thisptr.hello()

    def int_vec(self, v):
        """ int_vec(v): print vector v """
        self.thisptr.int_vec(v)
        
    def out_vec(self):
        cdef vector[double] out
        self.thisptr.out_vec(out)
        return out

    cpdef T_vec(self, object[np.double_t, ndim=2] v):
        if v.shape[1] != 3:
            raise ValueError("This is the 3D implementation! Your dimension is %s"%v.shape[1])
        cdef int n = v.shape[0]
        cdef vector[Map3d] cvec
        cvec.resize(n)
        cdef double* p = &v[0,0]
        for i in range(n):
            (cvec[i]).reset(p)
            p += 3
        self.thisptr.T_vec[Map3d](cvec)
    
    property i_:
        def __get__(self): return self.thisptr.i_
        def __set__(self, i_): self.thisptr.i_ = i_
