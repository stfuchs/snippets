#include <iostream>
#include <vector>
#include <Eigen/Core>

namespace mylib
{
  using namespace Eigen;
  
  struct MyClass
  {
    MyClass() : i_(0) {}
    MyClass(int i) : i_(i) {}
    int i_;
    Vector3f x;

    void hello()
    {
      std::cout << "Hello world " << i_ << std::endl;
    }

    void int_vec(std::vector<int> const& v)
    {
      for( auto it=v.begin(); it!=v.end(); ++it)
        std::cout << *it << ", ";
      std::cout << std::endl;
    }

    void out_vec(std::vector<double> & out)
    {
      out.resize(i_);
      for(int i=0; i<i_; ++i)
      {
        out[i] = i*.5-(i-1.);
      }
    }

    template<typename T, int R>
    void eig_vec(std::vector<Matrix<T,R,1> >& v)
    {    
      for( auto it=v.begin(); it!=v.end(); ++it)
        *it *= .5;
    }
    
    template<typename T>
    void T_vec(std::vector<T> const& v)
    {
      for( auto it=v.begin(); it!=v.end(); ++it)
        std::cout << *it << "\n";
      std::cout << std::endl;
    }
  };

  template<typename T>
  void ptr_vec(std::vector<T*> v)
  {
    for (size_t i=0; i<v.size(); ++i) { std::cout << *v[i] << std::endl; }
  }

  template<typename T>
  void print_arr(T* t, int n)
  {
    for (int i=0; i<n; ++i) { std::cout << t[i] << std::endl;}
  }
  
}
