#include <vector>
#include "eigenmap.cpp"

int main(int argc, char** argv)
{
  typedef EigenMap<double,3,1> Map3d;
  double d1[] = { .1, .2, .3 };
  double d2[] = { .4, .5, .6 };
  std::vector<Map3d> vec;
  vec.resize(2);
  //vec[0].reset(d1);
  //vec[1].reset(d2);
  vec[0] = Map3d(d1);
  vec[1] = Map3d(d2);
  std::cout << vec[0] << "\n";
  std::cout << vec[1] << "\n";
  return 0;
}
