#!/usr/bin/env python

from distutils.extension import Extension
from Cython.Build import cythonize

ext1 = Extension(
    "mylibpy",
    sources=["py/mylibpy.pyx", "src/mylib.cpp","src/eigenmap.cpp"],
    language="c++"
)
ext2 = Extension(
    "libeigenmap",
    sources=["py/libeigenmap.pyx", "src/eigenmap.cpp"],
    language="c++"
    )

cythonize([ext1])

