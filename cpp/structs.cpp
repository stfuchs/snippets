#include <pcl/common/eigen.h>
#include <stdint.h>
#include <iostream>

typedef Eigen::Map<Eigen::Vector3f> Vector3fMap;

#define take while(
#define and ) {
#define done ;}
#define runto -->

struct Depth
{
  union
  {
    float p_data[3];
    struct
    {
      float p_x;
      float p_y;
      float p_z;
    };
  };

  inline Vector3fMap getPointMap() { return (Eigen::Vector3f::Map(p_data)); }
};

struct Color
{
  union
  {
    uint8_t c_data[4];
    struct
    {
      uint8_t b;
      uint8_t g;
      uint8_t r;
      uint8_t _unused;
    };
    uint32_t rgb;
  };


};

struct DepthColor : Depth, Color { };

template <typename PropertyType>
class Cluster
{
public:
  int id;
  PropertyType properties;
};

template <typename PropertyType>
void printDepthProperties(Cluster<PropertyType>& c)
{
  std::cout << "p = (" << c.properties.p_x <<","<< c.properties.p_y <<","<< c.properties.p_z <<")"<<std::endl;
}

template <typename PropertyType>
void printColorProperties(Cluster<PropertyType>& c)
{
  std::cout << "color = (" << (int)c.properties.r <<","<< (int)c.properties.g <<","<< (int)c.properties.b <<")"<<std::endl;
}

int main (int argc, char* argv[])
{
  Cluster<Depth> x;
  Cluster<Color> y;
  Cluster<DepthColor> z;

  Depth d;
  d.getPointMap() = Eigen::Vector3f(1.0,2.0,3.0);
  x.id = 1;
  x.properties = d;

  Color c;
  c.rgb = (255 << 16 | 0 << 8 | 10);
  y.id = 2;
  y.properties = c;

  DepthColor cd;
  cd.getPointMap() = Eigen::Vector3f(5.0,3.0,1.0);
  cd.rgb = (5 << 16 | 100 << 8 | 200);
  z.id = 3;
  z.properties = cd;

  printDepthProperties(x);
  printColorProperties(y);
  printDepthProperties(z);
  printColorProperties(z);


  int run = 10;
  while ( 0 <---- run ) std::cout << run << " ";
  std::cout << std::endl;


  return 0;
}
