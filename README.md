# Mini Tuts

## Not so frequently used commands

ping server save time to file

     ping -c 600 server_ip | sed 's/.*time=\(.*\) ms/\1/' | cat >> out.txt

some ffmpeg stuff

     ffmpeg -ss 00:00:30 -t 00:00:05 -i orginalfile -c:v copy -c:a copy newfile
     ffmpeg -f image2 -i image%d.jpg -c:v libx264 -b 800k video.avi

rename sequence

       x=1;
       for i in *.ppm; do
           counter=$(printf %03d $x);
           ln -s "../$i" tmp/img"$counter".ppm;
           x=$(($x+1));
       done

### setup ncdc on DS213j (no ipkg)

get ARM binaries for [GNU screen](https://mocko.org.uk/projects/armbinaries/)
and [ncdc](http://dev.yorhel.nl/ncdc) and copy on DiskStation.
Then  set the following variables manually or in .profile

     export TERM='vt102'
     export TERMINFO='/usr/share/terminfo'

using ncdc with screen:

      screen -U -S ncdc /usr/bin/ncdc // start ncdc
      screen -U -r ncdc               // switch to ncdc
      <Ctrl> + <A>, <D> : detach screen and go back to prompt
      <Ctrl> + <A>, <:quit> : exit screen  


### print from command line:
[LPR Infos](http://www.math.fsu.edu/Computer/printer_cl.math)

    lpstat -p -d // list available printers and the default one
    lpoptions -p ${PRINTER_NAME} -l // lists printer options and default values
    lpr -P ${PRINTER_NAME} -o media=A4 ${PDF} // print

### using pulseaudio:
change sinks:

       pacmd list-sinks // list sinks to find index of desired device
       pacmd set-default-sink # // set default sink to index
       pacmd move-sink-input # # // move current active input to new device while playing
