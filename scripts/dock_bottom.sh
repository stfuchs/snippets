#!/bin/sh

active_id=$(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')
active_height=$(xwininfo -id $active_id | awk -F ' *: *' '/ (Height)/ { print $2 }')

set -- $(xwininfo -root | awk -F ' *: *' '/ (Width|Height):/ { print $2 }')
width=$1
height=$2

wmctrl -r :ACTIVE: -e 1,-1,$((height-active_height)),-1,-1