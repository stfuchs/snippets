#!/bin/sh

active_id=$(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')
active_width=$(xwininfo -id $active_id | awk -F ' *: *' '/ (Width)/ { print $2 }')
#echo $active_width
wmctrl -r :ACTIVE: -e 1,-1,-1,$((active_width-15)),-1