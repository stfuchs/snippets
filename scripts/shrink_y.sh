#!/bin/sh

active_id=$(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')
active_height=$(xwininfo -id $active_id | awk -F ' *: *' '/ (Height)/ { print $2 }')
#echo $active_height
wmctrl -r :ACTIVE: -e 1,-1,-1,-1,$((active_height-20))