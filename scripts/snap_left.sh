#!/bin/sh

set -- $(xwininfo -name "Bottom Expanded Edge Panel" | awk -F ' *: *' '/ (Absolut)/ { print $2 }')
bottom_abs_x=$1
bottom_abs_y=$2

#echo $bottom_abs_y

set -- $(xwininfo -name "Top Expanded Edge Panel" | awk -F ' *: *' '/ (Absolut|Height)/ { print $2 }')
top_abs_x=$1
top_abs_y=$2
top_height=$3
#echo $top_abs_y

set -- $(xwininfo -root | awk -F ' *: *' '/ (Width|Height):/ { print $2 }')
width=$1
height=$2
#echo $height

#echo $((bottom_abs_y- top_height))

wmctrl -r :ACTIVE: -e 1,0,0,$((width/2)),$((bottom_abs_y-top_height))
