function C_rot = gaussian_rotate(C, phi)

R = [[cos(phi) -sin(phi)]; [sin(phi) cos(phi)]]
C_rot = R * C * R';
