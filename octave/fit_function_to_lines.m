p = [ [-3 -3]; [1 1]; [4 1] ];
np = length(p);
rnp = 100;
irnp = 10;

sigma = 0.5;
px = zeros(rnp,np);
py = zeros(rnp,np);
for r=1:rnp
  px(r,:) = sigma * randn(1,np) + transpose(p(:,1));
  py(r,:) = sigma * randn(1,np) + transpose(p(:,2));
end

specs = ['-xr'];

pmin = floor( min( min(min(px)), min(min(py)) ) - 1);
pmax = floor( max( max(max(px)), max(max(py)) ) + 1);

X = zeros(rnp*irnp,1);
Y = zeros(rnp*irnp,1);
for r=1:rnp
  xx(r,:) = linspace(min(px(r,:)), max(px(r,:)), irnp);
  pv(r,:) = interp1(px(r,:), py(r,:), xx(r,:));
  d0 = (r-1)*irnp+1;
  d1 = r*irnp;
  X( d0:d1 ) = transpose(xx(r,:));
  Y( d0:d1 ) = transpose(pv(r,:));
end

A = feature_quad(X);
printf("A is of size %ix%i\n", size(A));
beta = inv(transpose(A) * A) * transpose(A) * Y;

xi = transpose(linspace(pmin,pmax,100));
yi = feature_quad(xi) * beta;

clf;
hold on
axis([pmin pmax pmin pmax])
axis square
for i=1:rnp
  %plot( px(i,:), py(i,:), specs( rem(i,size(specs)(1))+1,:) );
  plot( xx(i,:), pv(i,:), 'xr' );
end
plot( transpose(p(:,1)), transpose(p(:,2)), '-ob')
plot( xi, yi, 'm')
hold off

