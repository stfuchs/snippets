function [mu C] = gaussian_product(mu1, C1, mu2, C2)

C1_inv = inv(C1);
C2_inv = inv(C2);

C = inv(C1_inv + C2_inv);
mu = C * (C1_inv * mu1 + C2_inv * mu2);
