function [out] = feature_quad(X)

n = size(X)(1);
d = size(X)(2);
m = d + 1 + 0.5 * d * (d+1);
out = zeros(n,m);
idx = 1;
for i=1:d
  for j=i:d
    out(:,idx) = X(:,i) .* X(:,j);
    idx++;
  end
end
out(:,m-d:end) = feature_lin(X);

end
