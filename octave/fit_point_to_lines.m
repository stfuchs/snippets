p = [ [-3 -3]; [1 1]; [4 -2]; [6 3] ];
np = length(p); % number of planes
rnp = 10; % number of random planes
irnp = 10; % number of interpolations of random planes
sigma = 0.5;

px = zeros(rnp+1,np);
py = zeros(rnp+1,np);
for r=1:rnp
  px(r,:) = sigma * randn(1,np) + transpose(p(:,1));
  py(r,:) = sigma * randn(1,np) + transpose(p(:,2));
end
  px(r,:) = sigma * randn(1,np) + transpose(p(:,1)) - 1;
  py(r,:) = sigma * randn(1,np) + transpose(p(:,2)) + 2;

specs = ['-xr'];

pmin = floor( min( min(min(px)), min(min(py)) ) - 1);
pmax = floor( max( max(max(px)), max(max(py)) ) + 1);

Coef = zeros((np-1)*rnp,3);
Q = zeros(3);
for r=1:rnp
  for l=1:np-1
    idx = (r-1)*rnp + l;
    nx = py(r,l+1) - py(r,l);
    ny = - px(r,l+1) + px(r,l);
    len_inv = 1.0/sqrt(nx^2 + ny^2);
    nx = nx * len_inv;
    ny = ny * len_inv;
    d = -(px(r,l) * nx + py(r,l) * ny);
    Coef(idx,:) = [nx ny d];
    Q += transpose([nx ny d]) * [nx ny d];
  end
end

%Q = transpose(Coef) * Coef;
Q_inv = inv([Q(1:2,:); [0 0 1] ]);
v = Q_inv * [0;0;1];

clf;
hold on
axis([pmin pmax pmin pmax])
axis square
for i=1:rnp
  plot( px(i,:), py(i,:), specs( rem(i,size(specs)(1))+1,:) );
end
plot( transpose(p(:,1)), transpose(p(:,2)), '-ob')
plot( v(1), v(2), 'sm')
plot( Coef(:,1) , Coef(:,2), 'xm')
plot( 0, 0, 'sk')

hold off

