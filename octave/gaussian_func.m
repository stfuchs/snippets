function [G A] = gaussian_func(mu, C, xgrid, ygrid)

xx = size(xgrid)(2);
yy = size(xgrid)(1);
f = zeros(xx,yy);
C_inv = inv(C);
A = 1/sqrt(det(2*pi*C));
G = zeros(xx,yy);

for ix=1:xx
    for iy=1:yy
        demean = [xgrid(ix,iy);ygrid(ix,iy)] - mu;
        G(ix,iy) = A * exp(-(demean' * C_inv * demean)/2);
    end
end



