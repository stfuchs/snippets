C1 = [[5 0];...
      [0 .1]];
mu1 = [0;0];

C2 = [[1 0];...
      [0 .1]];
mu2 = [5;0];
C2 = gaussian_rotate(C2, pi/2);

[mu3 C3] = gaussian_product(mu1,C1,mu2,C2);
C1 = [[5 0];...
      [0 .1]];
[mu4 C4] = gaussian_product(mu1,C1,mu3,C3);
C1 = [[2.5 0];...
      [0 .1]];
[mu5 C5] = gaussian_product(mu1,C1,mu4,C4);
C1 = [[1 0];...
      [0 .1]];
[mu6 C6] = gaussian_product(mu1,C1,mu5,C5);
[mu7 C7] = gaussian_product(mu1,C1,mu6,C6);
[mu7 C7] = gaussian_product(mu1,C1,mu7,C7);
[mu7 C7] = gaussian_product(mu1,C1,mu7,C7);
[mu7 C7] = gaussian_product(mu1,C1,mu7,C7);
[mu7 C7] = gaussian_product(mu1,C1,mu7,C7);
[mu7 C7] = gaussian_product(mu1,C1,mu7,C7);
C1 = [[5 0];...
      [0 .1]];

C2 = [[.5 0];...
      [0 .5]];

C4 = C1+C2;

[xgrid, ygrid] = meshgrid( -7:0.1:7, -7:0.1:7);

[G1 A1] = gaussian_func(mu1,C1,xgrid,ygrid);
[G2 A2] = gaussian_func(mu1,C2,xgrid,ygrid);
G3 = .5*G1 + .5*G2;
[G4 A4] = gaussian_func(mu1,C4,xgrid,ygrid);
%[G3 A3] = gaussian_func(mu3,C3,xgrid,ygrid);
%[G4 A4] = gaussian_func(mu4,C4,xgrid,ygrid);
%[G5 A5] = gaussian_func(mu5,C5,xgrid,ygrid);
%[G6 A6] = gaussian_func(mu6,C6,xgrid,ygrid);
[G7 A7] = gaussian_func(mu7,C7,xgrid,ygrid);


clf;
hold on
contour( xgrid, ygrid, G1, [0.05 0.05], 'r');
contour( xgrid, ygrid, G2, [0.05 0.05], 'm');
contour( xgrid, ygrid, G3, [0.05 0.05], 'b');
contour( xgrid, ygrid, G4, [0.05 0.05], 'g');
%contour( xgrid, ygrid, G5, [0.05 0.05], 'r');
%contour( xgrid, ygrid, G6, [0.05 0.05], 'c');
contour( xgrid, ygrid, G7, [0.05 0.05], 'c');
%surf(xgrid,ygrid,G3);
%surf(xgrid,ygrid,G6);
axis('equal')
