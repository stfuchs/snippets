
x   = [0 1 2 3 4 5 6 7 8 9];
mu1 = [nan 5 4 3 2 2 2 2 nan nan];
C1 = [nan 1 .5 .2 .1 .1 .1 .1 nan nan];
mu2 = [nan nan nan nan nan 2.15 2.1 2.05 2 nan];
C2 = [nan nan nan nan nan .5 .2 .2 .1 nan];

C3 = 1./(1./C1 + 1./C2);
mu3 = C3.*(1./C1.*mu1 + 1./C2.*mu2);


clf;
hold on
%plot(x,mu1,'rx')
errorbar(x,mu1,C1,'~rx')
errorbar(x,mu2,C2,'~bo')
errorbar(x,mu3,C3,'~mo')
%plot(x,mu2,'bo')
%errorbar(x,C2,'bo')
axis([-1 10 0 6])
